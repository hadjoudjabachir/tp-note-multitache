#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <pthread.h>
#include <unistd.h>
#include "calcul.h"
using namespace std;

struct predicatRdv {
// regroupes les donnée partagées entres les threads participants aux RdV :
int* cpt;
pthread_mutex_t * verrou_cpt;
pthread_cond_t * condi;

};

struct params {
  // structure pour regrouper les paramètres d'un thread.   

  int idThread; // un identifiant de thread, de 1 à N (N le nombre	
  // total de theads secondaires
  struct predicatRdv * varPartagee;
};

// fonction associée a chaque thread participant au RdV.

void * participant (void * p){ 

  // On cast params (de type void) en paramsFonctionThread
  struct params * args = (struct params *) p;
  struct predicatRdv * predicat = args -> varPartagee;
    printf("working!\n");
  sleep(1);

  printf("Mon id est :  %i\n", (*args).idThread); 

    // Je vérouille l'accès à ma variable cpt

 /*--lock-->*/   pthread_mutex_lock(args->varPartagee->verrou_cpt);

    printf("hello, j'ai un cpt de :%i (before)\n",*(args->varPartagee->cpt));
    (*(predicat->cpt))--;
    printf("hello, j'ai un cpt de :%i (after)\n",*(args->varPartagee->cpt));

    if(*(predicat->cpt)!=0)
    {
    //le wait prends une condition et un verrou, il doit etre entre lock et unlock
      pthread_cond_wait(args->varPartagee->condi, args->varPartagee->verrou_cpt);
    }
    else
    {
      //brodcast envoie un message a tous les threads en attente
      pthread_cond_broadcast(args->varPartagee->condi);
    }
  //et puis dès qu'on fait appelle a la condition (genre fin de wait) le wait relock le verrou.
  /*--unlock-->*/   pthread_mutex_unlock(args->varPartagee->verrou_cpt);
    
    printf("Ah shit, here we go again!\n");
  return 0;
}
  /*// simulation d'un long calcul pour le travail avant RdV
  calcul (args -> idThread + rand() % 10); // c'est un exemple.

  // RdV 

  while () {  // attention : le dernier arrivé ne doit pas attendre. Il doit réveiller tous les autres.
   attente
  }


 
  calcul (); // reprise et poursuite de l'execution.



  pthread_exit(NULL); // fortement recommandé.
}*/




int main(int argc, char * argv[]){
  
  if (argc!=2) {
    cout<<" argument requis "<<endl;
    cout<<"./prog nombre_Threads"<<endl;
    exit(1);
  }

 
  // initialisations 
  pthread_t threads[atoi(argv[1])];

   // Tableau de paramètres
  struct params tabParams[atoi(argv[1])];
    
  // Variables communes à tous mes threads : cpt et verrou_cpt
  predicatRdv varCom;


  int cpt = atoi(argv[1]);
  varCom.cpt=&cpt;
  //srand(atoi(argv[1]));  // initialisation de rand pour la simulation de longs calculs
  
  pthread_mutex_t verrou_cpt;
  pthread_mutex_init(&verrou_cpt, NULL);
  varCom.verrou_cpt = &verrou_cpt;

  pthread_cond_t condi;
  pthread_cond_init(&condi,NULL);
  varCom.condi=&condi;


  // création des threards 
  for (int i = 0; i < atoi(argv[1]); i++){
    tabParams[i].idThread = i;
    tabParams[i].varPartagee = &varCom; // On passe le même verrou à tous les threads

    if (pthread_create(&threads[i], NULL, participant, &tabParams[i]) != 0){
      perror("erreur creation thread");
      exit(1);
    }
  }
    printf("creation de thread reussie\n");


  // attente de la fin des  threards. Partie obligatoire 
  for (int i = 0; i < atoi(argv[1]); i++){
  pthread_join(threads[i],NULL);
    }
  cout << "thread principal : fin de tous les threads secondaires" << endl;

  // terminer "proprement". 
  
  printf("Tous tes threads ont répondu, valeur de cpt : %i\n", cpt);

return 0; 
}
 
