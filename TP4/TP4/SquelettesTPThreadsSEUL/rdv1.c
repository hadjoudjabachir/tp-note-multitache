#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include "calcul.h"


struct paramsFonctionThread {
  int idThread;
  int waitTime;
  int * cpt;
  pthread_mutex_t * verrou_cpt;
};


void * fonctionThread (void * params){

  // On cast params (de type void) en paramsFonctionThread
  struct paramsFonctionThread * args = (struct paramsFonctionThread *) params;
  printf("Appel depuis fonctionThread\n");
  /*
  * Les deux lignes font la même choses : 
  * (*args).idThread 
  * args->idThread
  */
  printf("Id thread %lu : %i, proc : %i\n", pthread_self(),args->idThread, getpid());

  printf("Début calcul thread %i\n", args->idThread);
  calcul(args->waitTime);
  // Je vérouille l'accès à ma variable cpt
  pthread_mutex_lock(args->verrou_cpt);
  (*(args->cpt))++;
  pthread_mutex_unlock(args->verrou_cpt);
  return 0;
    /*Question 3 : cet exit ferme le programme ; les autres threads ne finissent pas et l'exécution 
    ne dépasse pas cette étape. 
    if (args->waitTime > 1) {
        exit(EXIT_FAILURE);
    }*/

  printf("Fin calcul thread %i\n", args->idThread);

    int* ret = malloc(sizeof(int));
    *ret = args->idThread;
    pthread_exit(ret);
}


int main(int argc, char * argv[]){

  if (argc < 2 ){
    printf("utilisation: %s  nombre_threads  \n", argv[0]);
    return 1;
  }     

  //tableau qui sert pour stockes les identifiants des threads
  pthread_t threads[atoi(argv[1])];

  // Tableau de paramètres
  struct paramsFonctionThread params[atoi(argv[1])];
 
    // Variables communes à tous mes threads : cpt et verrou_cpt
  int cpt = 0;
  pthread_mutex_t verrou_cpt;
  pthread_mutex_init(&verrou_cpt, NULL);

  // création des threards 
  for (int i = 0; i < atoi(argv[1]); i++){
    // Le passage de paramètre est fortement conseillé (éviter les
    // variables globles).
        params[i].idThread = i;
        params[i].verrou_cpt = &verrou_cpt; // On passe le même verrou à tous les threads
        params[i].cpt = &cpt; // On passe le même compteur
        params[i].waitTime = i;
        
        printf("Entrée boucle thread %i\n", i);

 // compléter pour initialiser les paramètres
    if (pthread_create(&threads[i], NULL,fonctionThread,&params[i]) != 0){
      perror("erreur creation thread");
      exit(1);
    }
          printf("Sortie boucle thread %i\n", i);
  }
    /*int res;
    int* ret;
    for (int i = 0; i < atoi(argv[1]); i++) {
        if ((res = pthread_join(threads[i], (void**) &ret)) != 0) {
            printf("Erreur %i durant join thread %i\n", res, i);
        }
        printf("Le résultat du thread %i est %i\n", i, *ret);
        free(ret);
    }*/
      // On attend la fin des threads
      for (int i = 0; i < atoi(argv[1]); i++){
        pthread_join(threads[i],NULL);
      }

    printf("Tous tes threads ont répondu, valeur de cpt : %i\n", cpt);
    printf("Cette ligne doit être la dernière ligne du programme\n");

    return 0;
 
}
 
