#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include<arpa/inet.h>
#include<string.h>
#include<pthread.h>

//Paramètres propres à chaque thread
struct paramsThread {
   int dsClient;
};

void* traitement(void* p){
   struct paramsThread* args = (struct paramsThread *) p;
   int client = args->dsClient;

   char message[4000];
   int lenn_msg; 
   
   
   int receptionTaille=recv(client,&lenn_msg,sizeof(int),0);
   
   if(receptionTaille == -1){ 
      perror("Serveur :erreur lors de la réception du message\n ");
      exit(1);
   }
   printf("%d est la taille du message\n", lenn_msg);

   int reception=recv(client,message,sizeof(message),0);
     

   if(reception == -1){ 
      perror("Serveur :erreur lors de la réception du message\n ");
      exit(1);
   }
   
   printf("nombre d'octets recus : %d\n", reception);
   printf("message recu :%s\n appuyez sur entrée pour passer à l'envoi au client \n ",message);
   getchar();
 
  /* Etape 6 : envoyer un message au client (voir sujet pour plus de détails)*/


  int res= send(client,&reception,sizeof(reception), 0);
  if(res==-1)
  {
     perror("Serveur : probleme lors de l'envoi du message \n");
     exit(1);

  }else if (res==0) {
     printf("Serveur TCP :la socket a été fermée");

  }  
   printf("%d est le nombre d'octets envoyé\n ",res);
   printf("le serveur a envoyé le message suivant au client %d\n",reception);
   
   close(client);
   pthread_exit(NULL);
}

int main(int argc, char *argv[]) {

  
   int  res;
  if (argc != 2){
    printf("utilisation : %s port_serveur\n", argv[0]);
    exit(1);
   }

  /* Etape 1 : créer une socket */   
  int socketServer = socket(PF_INET, SOCK_STREAM, 0);

   if (socketServer == -1){
      perror("Serveur : pb creation socket :");
      exit(1); 
  }
  
  printf("Serveur : creation de la socket réussie ,appuyer sur entrée pour passer au nommage \n");
  getchar();
  
  /* Etape 2 : Nommer la socket du seveur */
  /*On commence par configurer l'adresse de mon serveur */
  struct sockaddr_in addrServer;
  addrServer.sin_family=AF_INET;
  addrServer.sin_addr.s_addr=INADDR_ANY; //on utilise l'adresse locale à mon pc car je fais des tests  en interne
  addrServer.sin_port=htons((short) atoi(argv[1]));

  res=bind(socketServer ,(const struct sockaddr*) &addrServer ,sizeof(addrServer));

  if(res==-1)
   {
    perror("Serveur :problème lors du nommage de la socket\n ");
    exit(1);
   } 

  printf("Socket bien nommé ,appuyez sur entrée pour passer la socket en mode écoute\n");
  getchar();

   /*Etape 3:passer la socket en mode écoute */

   int ecoute=listen(socketServer,10);
   if(ecoute==-1)
   {
      perror("Serveur TCP :erreur lors de la mise sur écoute \n");
      exit(1);
   }

   printf("le serveur a été mis sur ecoute avec succes , pressez entrée pour passer à l'accord de connexion \n ");
   getchar();
   
   //pthread_t threads[10];
   pthread_t thread;
   //int i=0;

   while(1){
   /*Etape 4 :Accepter une connexion */
   struct sockaddr_in adClient;
   socklen_t lgA=sizeof(adClient);
   
   int dsClient=accept(socketServer,(struct sockaddr *)&adClient,&lgA);


   if(dsClient==-1)
   {
    perror("Serveur TCP : erreur lors de l'acceptation de connexion ");
    exit(1);
   }

   printf("accord de connexion réussi ,pressez entrée pour passer à l'attribution du thread\n ");
   getchar();

  /* Etape 5 : recevoir un message du client:on sait au préalable que c'est une chaine de caractere de taille max 4000*/
   struct paramsThread params;
   params.dsClient=dsClient;

   if (pthread_create(&thread,NULL,traitement,&params)!=0){
      perror("erreur création thread\n");
      exit(1);
   };

   //i++;
   }
   //for (int j=0; j<10;j++){
   pthread_join(thread, NULL);
   //}

  /* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/
  close(socketServer);

  printf("Serveur : je termine\n");

return 0;

}