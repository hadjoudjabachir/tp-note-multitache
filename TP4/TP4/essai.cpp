#include <string.h>
#include <stdio.h>//perror
#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <pthread.h>
using namespace std;

/*void *monThread(void *par){
    pthread_t moi = pthread_self();
    cout<<"monThread : "<<moi<<", proc. : "<<getpid()<<endl;
    sleep(3);
    //calculDureeSec(3);
    cout<<"monThread : fin"<<endl;
    pthread_exit(NULL);
}
int main(){
    pthread_t idTh;
    if(pthread_create(&idTh, NULL, monThread,NULL) != 0)
        cout<<"erreur creation"<<endl;
    int res = pthread_join(idTh, NULL);
    cout<<"principal : fin \n"<<endl;
}*/
pthread_mutex_t * verrou_cpt;

void *T1 (void *par){
    int * cp = (int *)(par);
    for(int i=0; i<1500; i++) ++(*cp);
    pthread_exit(NULL);
}

void *T2 (void *par){
    int * cp = (int *)(par);
    for(int i=0; i<3000; i++) ++(*cp);
    pthread_exit(NULL);
}
int main(){
    pthread_t idT1, idT2;
    int counter =0;
    //pthread_mutex_t verrou_cpt;
    //pthread_mutex_init(&verrou_cpt, NULL);


    if(pthread_create(&idT1, NULL, T1,&counter) != 0)
        cout<<"erreur creation"<<endl;
    if(pthread_create(&idT2, NULL, T2,&counter) != 0)
        cout<<"erreur creation"<<endl;

    int res = pthread_join(idT1, NULL);
    res = pthread_join(idT2, NULL);
    cout<<"Total = "<<counter<<endl;

    return 0;
}
