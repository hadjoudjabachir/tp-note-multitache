#include <string.h>
#include <stdio.h>//perror
#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <pthread.h>


// Paramètres de ma fonction threads. Pour transmettre une infromation à un thread, c'est ici !
struct paramsFonctionThread {
  int idThread;
  struct variableCommune* varCommune;
};

struct variableCommune {
    int * nbZone;
    int * di;
    pthread_mutex_t * verrou;
    pthread_cond_t * condi;
};


void * traitement (void * p) {

  struct paramsFonctionThread * args = (struct paramsFonctionThread *) p;
  //struct  variableCommune *  varCommune = args -> varCommune; rename (sucre syntaxique)

  for(int i = 0; i <= *(args->varCommune->nbZone); i++){
    printf("Mon id est :  %i\n", (*args).idThread); 
    if( args -> idThread != 0){ // le premier traitement n'attent personne
      pthread_mutex_lock(args->varCommune->verrou);
      printf("Je suis verrouille...\n");
      if(args->varCommune->di[args->idThread-1]<=i)
      {
        printf("Mise en attente :/ Je suis de la zone: %i \n",i);
        pthread_cond_wait(&args->varCommune->condi[args->idThread-1], args->varCommune->verrou);
      }
      pthread_mutex_unlock(args->varCommune->verrou);
      //mettre a jour le tableau
      pthread_mutex_lock(args->varCommune->verrou);
      printf("Mise a jour du tableau\n");
      sleep(1);
      args->varCommune->di[args->idThread]++;
      //le dire a tout le monde 
      pthread_mutex_unlock(args->varCommune->verrou);
      pthread_cond_signal(&args->varCommune->condi[args->idThread]);
    }
  }

 
  pthread_exit(NULL); 
}




int main(int argc, char * argv[]){
  
  if (argc!=3) {
    std::cout << " argument requis " << std::endl;
    std::cout << "./prog nombre_Traitements nombre_Zones" << std::endl;
    exit(1);
  }

 
   // initialisations 
  pthread_t threads[atoi(argv[1])];
  struct paramsFonctionThread tabParams[atoi(argv[1])];

  struct variableCommune varCommune;

  //int var1[atoi(argv[1])]
  varCommune.di=(int*)malloc(sizeof(int)*atoi(argv[1]));
  for(int i=0;i<atoi(argv[1]);i++)
  {
    varCommune.di[i]=0;
  }
  int var=atoi(argv[2]);
  varCommune.nbZone = &var;

  varCommune.condi=(pthread_cond_t*)malloc(sizeof(pthread_cond_t)*atoi(argv[1])-1);
  for(int i=0;i<atoi(argv[1])-1;i++)
  {
    pthread_cond_init(&varCommune.condi[i],NULL);
  }

  pthread_mutex_t verrou;
  pthread_mutex_init(&verrou, NULL);
  varCommune.verrou = &verrou;
  

  
  srand(atoi(argv[1]));  // initialisation de rand pour la simulation de longs calculs
 
  // creation des threards 
  for (int i = 0; i < atoi(argv[1]); i++){
    tabParams[i].idThread = i;
    tabParams[i].varCommune = &varCommune; 
    if (pthread_create(&threads[i], NULL, traitement, &tabParams[i]) != 0){
      perror("erreur creation thread");
      exit(1);
    }
  }

  
  // attente de la fin des  threards. Partie obligatoire 
  for (int i = 0; i < atoi(argv[1]); i++){
    pthread_join(threads[i],NULL);
  }
  std::cout << "thread principal : fin de tous les threads secondaires" << std::endl;

  // liberer les ressources avant terminaison 

  for(int i=0;i<atoi(argv[1]-1);i++)
  {
    pthread_cond_destroy(&varCommune.condi[i]);
  }

  pthread_mutex_destroy(varCommune.verrou);
  return 1;
}
/*
struct variableCommune {
    int * nbZone;
    int * di;
    pthread_mutex_t * verrou;
    pthread_cond_t * condi;
};*/


//julie.cailler@lirmm.fr