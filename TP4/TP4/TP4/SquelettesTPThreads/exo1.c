#include <string.h>
#include <stdio.h>//perror
#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
//#include <iostream>
#include <pthread.h>
#include "calcul.h"

struct paramsFonctionThread {
  //ce n'est pas équivalent pthread_self
  int idThread;
  int* pdonnee;
  // si d'autres paramètres, les ajouter ici.
};

//faire des printf avant et au cours de l'exécution pour montrer que c'est parallèle
//Utiliser la fonction de calcul qui affiche le max du temps des 2 processus
//des boucles par exemple aussi
//pthread_self pour savoir quel thread on utilise et après on lui donne un indice, pour pas appeler pthread_self à chaque fois 
//séquentiel veut dire que quand l'un termine l'autre se lance 

void * fonctionThread (void * params){
  struct paramsFonctionThread * args = (struct paramsFonctionThread *) params;
  pthread_t moi = pthread_self();
  printf("indice : %i, monThread : %ld, proc : %d\n",args->idThread, moi, getpid());
  //calcul(3);
  printf("MonThread : fin\n");
  pthread_exit(NULL);
}


int main(int argc, char * argv[]){

  if (argc < 2 ){
    printf("utilisation: %s  nombre_threads  \n", argv[0]);
    return 1;
  }     
  //tableau qui sert pour stockes les identifiants des threads
  pthread_t threads[atoi(argv[1])];

  //il faut mettre un pthread_create
  int donne_partagee=10;
  // création des threards 
  for (int i = 0; i < atoi(argv[1]); i++){

    // Le passage de paramètre est fortement conseillé (éviter les
    // variables globles).

      //struct paramsFonctionThread thread; 
      //UNE ERREUR ! ça on l'aura plus après la fin de boucle et on pourra pas s'en servir dans la fonction fonctionThread
      // solution : utiliser malloc struc paramsFonctionThread * malloc .........
      
        struct paramsFonctionThread * thread= malloc(sizeof(struct paramsFonctionThread));
        thread->idThread=i;
        thread->pdonnee=&donne_partagee;
        //int* thread = malloc(sizeof(int));
        //*thread = i;  
         // compléter pour initialiser les paramètres
         // premier argument de pthread_create : paramètre résultat qui récupère l'identifiant du thread créé, pour but d'attendre la fin de l'éxécution d'un thread (j'ai besoin de son identifiant d'où l'utilisation de l'identifiant qui m'a été retourné quand j'ai créé un thread)
    if (pthread_create(&threads[i], NULL,fonctionThread, thread) != 0){
      perror("erreur creation thread");
      exit(1);
    }
  }


// garder cette saisie et modifier le code en temps venu.
 /* char c[3]; 
  printf("saisir un caractère \n");
  fgets(c, 1, stdin);

 //... compléter
*/
  return 0;
 
}
