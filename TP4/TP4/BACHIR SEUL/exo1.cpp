#include <string.h>
#include <stdio.h>//perror
#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <pthread.h>
using namespace std;

//variables propres pour chaque thread
struct paramsFonctionThread {

  int idThread;
  int* cpt;
  pthread_mutex_t * verrou;

};


void * fonctionThread (void * params){

  struct paramsFonctionThread * args = (struct paramsFonctionThread *) params;
  printf("[Thread %i]: %lu, j'appartiens au processus : %i\n", args->idThread, pthread_self(),getpid());
  sleep(5);

  pthread_mutex_lock(args->verrou);
  printf("[Thread %i]: mon compteur avant : %i\n", args->idThread, *(args->cpt));
  (*(args->cpt))++;
  printf("[Thread %i]: mon compteur après : %i\n", args->idThread, *(args->cpt));
  pthread_mutex_unlock(args->verrou);
  
  printf("[Thread %i]: j'ai fini mon calcul\n", args->idThread);
  pthread_exit(NULL);
}


int main(int argc, char * argv[]){

  if (argc < 2 ){
    printf("utilisation: %s  nombre_threads  \n", argv[0]);
    return 1;
  }     

  //initialisation du tableau des threads
  pthread_t threads[atoi(argv[1])];
  //initialisation du tableau des paramètres
  struct paramsFonctionThread tabParams[atoi(argv[1])];
  

 
  int cpt =0;
  pthread_mutex_t verrou;
  pthread_mutex_init(&verrou, NULL);

  // création des threards 
  for (int i = 0; i < atoi(argv[1]); i++){

    // Le passage de paramètre est fortement conseillé (éviter les
    // variables globles).
    tabParams[i].idThread=i;
    tabParams[i].cpt=&cpt;
    tabParams[i].verrou=&verrou;

    // compléter pour initialiser les param�tres
    if (pthread_create(&threads[i], NULL,fonctionThread, &tabParams[i]) != 0){
      perror("erreur creation thread");
      exit(1);
    }
  }

  for (int i = 0; i < atoi(argv[1]); i++){
    pthread_join(threads[i],NULL);
  }


  printf("Tous tes threads ont répondu, valeur de cpt : %i\n", cpt);


/*// garder cette saisie et modifier le code en temps venu.
  char c; 
  printf("saisir un caract�re \n");
  fgets(m, 1, stdin);

 ... compl�ter*/

  return 0;
 
}
 
