#include <iostream>
#include <sys/types.h>
#include <pthread.h>
#include <unistd.h>
#include "calcul.h"
using namespace std;

// structure qui regroupe les variables partagées entre les threads.
struct varPartagees {
  int nbZones;
  int * di; // le tableau indiqué dans l'énoncé
  pthread_mutex_t verrou;
  pthread_cond_t cond;

};

// structure qui regroupe les paramètres d'un thread
struct params {

  int idThread; // cet entier correspond au numéro de traitement associé à un thread 
  struct varPartagees * vPartage;
};

// fonction associée à chaque thread secondaire à créer.

void * traitement (void * p) {

  struct params * args = (struct params *) p;
  struct  varPartagees *  vPartage = args -> vPartage;


  for(int i = 0; i <= vPartage -> nbZones; i++){
    printf("[Thread %i] je veux traiter la zone: %i \n",args->idThread, i);
      // Je regarde si je peux traiter la zone que je veux traiter (la zone i)
    pthread_mutex_lock(&(vPartage->verrou)); 
    if( args -> idThread != 0){ // le premier traitement n'attend personne

      //Il se met en attente tant que la ressource n'est pas dispo
      while(vPartage->di[args->idThread-1]<= i){ 
      // attente fin traitement sur la zone i 
      printf("[Thread %i] mise en attente pour traiter la zone: %i \n",args->idThread, i);
      pthread_cond_wait(&vPartage->cond, &vPartage->verrou);
      }
    }
      //il n'est plus en attente, la fin du wait (pour ceux qui sont pas les premiers) vérouille again donc on doit unlocker, et le lock pour le premier
    pthread_mutex_unlock(&vPartage->verrou);


    // dans cette partie, le traitement de la zone i est à faire en faisant une simulation d'un long calcul (appel a calcul(...)
    printf("[Thread %i] : Maintenat je vais traiter la zone : %i \n",args->idThread, i);
    //traitement du thread
    sleep(5);
    
    // Mise à jour du tableau à la fin du traitement
    pthread_mutex_lock(&(vPartage->verrou));
    vPartage->di[args->idThread]++; 
    pthread_mutex_unlock(&(vPartage->verrou));

    // a la fin du traitement d'une zone, le signaler pour qu'un thread en attente se réveille. 
    printf("[Thread %i] : j'ai fini de traiter la zone: %i, je notifie tout le monde \n",args->idThread, i);
    pthread_cond_broadcast(&(vPartage->cond));
}

  pthread_exit(NULL); 
}




int main(int argc, char * argv[]){
  
  if (argc!=3) {
    cout << " argument requis " << endl;
    cout << "./prog nombre_Traitements nombre_Zones" << endl;
    exit(1);
  }

 
   // initialisations 
  pthread_t threads[atoi(argv[1])];
  struct params tabParams[atoi(argv[1])];
  //créer et initialiser la vartiable partagee
  struct varPartagees vPartage;
  //verrou
  pthread_mutex_t verrou;
  pthread_mutex_init(&verrou, NULL);
  vPartage.verrou=verrou;
  //cond
  pthread_cond_t cond;
  pthread_cond_init(&cond, NULL);
  vPartage.cond=cond;
  //NbZones
  vPartage.nbZones =  atoi(argv[2]);

  // di : création + initialisation
  vPartage.di = (int*) malloc(sizeof(int)*atoi(argv[1]));
  for (int i=0; i<atoi(argv[1]);i++){
    vPartage.di[i]=0;
  }
  srand(atoi(argv[1]));  // initialisation de rand pour la simulation de longs calculs
 
  // création des threards 
  for (int i = 0; i < atoi(argv[1]); i++){
    tabParams[i].idThread = i;
    tabParams[i].vPartage = &vPartage; 
    if (pthread_create(&threads[i], NULL, traitement, &tabParams[i]) != 0){
      perror("erreur creation thread");
      exit(1);
    }
  }

  
  // attente de la fin des  threards. Partie obligatoire 
  for (int i = 0; i < atoi(argv[1]); i++){
    pthread_join(threads[i],NULL);
  }
  cout << "thread principal : fin de tous les threads secondaires" << endl;

  // libérer les ressources avant terminaison 
  pthread_cond_destroy(&(vPartage.cond));
  pthread_mutex_destroy(&(vPartage.verrou));
  free(vPartage.di);
  return 1;
}
 
