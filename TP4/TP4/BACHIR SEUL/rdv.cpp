#include <iostream>
#include <sys/types.h>
#include <pthread.h>
#include <unistd.h>
#include "calcul.h"
using namespace std;

struct predicatRdv {

// regrouoes les donn�e partag�es entres les threads participants aux RdV :
int cpt;
pthread_cond_t* tous_arrives;
pthread_mutex_t* lock;

};

struct params {

  // structure pour regrouper les param�tres d'un thread. 

  int idThread; // un identifiant de thread, de 1 � N (N le nombre
		// total de theads secondaires
  struct predicatRdv * varPartagee;


};

// fonction associ�e a chaque thread participant au RdV.

void * participant (void * p){ 

  struct params * args = (struct params *) p;
  struct predicatRdv * predicat = args -> varPartagee;
  printf("[Thread %i] : je vais commencer à travailler\n", args->idThread);
  // simulation d'un long calcul pour le travail avant RdV
  calcul (args -> idThread + rand() % 10); // c'est un exemple.
  printf("[Thread %i] : j'aurais besoin de la variable partagée donc je vais vérouiller\n", args->idThread);

  pthread_mutex_lock(predicat->lock);
  printf("[Thread %i] : cpt before : %i\n", args->idThread, predicat->cpt);
  predicat->cpt--;
  printf("[Thread %i] : cpt after : %i\n", args->idThread, predicat->cpt);
  //Le dernier predicat n'attend personne
  if(predicat->cpt == 0){
    printf("[Thread %i] : je suis le dernier et je vais réveiller tout le monde!\n", args->idThread);
    pthread_cond_broadcast(predicat->tous_arrives);
  }
  // RdV
  while (predicat->cpt!=0) {  // attention : le dernier arrivé ne doit pas attendre. Il doit réveiller tous les autres.
  printf("[Thread %i] : j'ai fini mon travail et je me mets en attente et je déverouille momentanément\n", args->idThread);
   pthread_cond_wait(predicat->tous_arrives, predicat->lock);
   printf("J'arrete d'attendre et je revérouille\n");
  }
  //je déverouille ce qui a été locké par lock pour le dernier, et par la fin d'attente pour le reste
  pthread_mutex_unlock(predicat->lock);
  printf("[Thread %i]: je dévérouille\n", args->idThread);

  // reprise et poursuite de l'execution.
  printf("[Thread %i]: calcul final sans variable globale\n", args->idThread);
  calcul (1); // c'est un exemple.
  
  printf("[Thread %i]: LA FIN!\n", args->idThread);

  pthread_exit(NULL); // fortement recommand�.
}




int main(int argc, char * argv[]){
  
  if (argc!=2) {
    cout << " argument requis " << endl;
    cout << "./prog nombre_Threads" << endl;
    exit(1);
  }

 
  // initialisations

  // initialisation du tableau de threads
  pthread_t threads[atoi(argv[1])];
  // initialisation du tableau des paramètres propres à chaque thread 
  struct params tabParams[atoi(argv[1])];



  //créer le verrou et la condition
  pthread_mutex_t lock;
  pthread_cond_t tous_arrives;

  //allouer (initialiser)le verrou et la condition
  pthread_mutex_init(&lock, NULL);
  pthread_cond_init(&tous_arrives, NULL);

  //créer et initialiser la variable partagée
  struct predicatRdv predicat;
  int cpt = atoi(argv[1]);
  predicat.cpt=cpt;
  predicat.lock=&lock;
  predicat.tous_arrives=&tous_arrives;

  srand(atoi(argv[1]));  // initialisation de rand pour la simulation de longs calculs
 
  // cr�ation des threards 
  for (int i = 0; i < atoi(argv[1]); i++){
    tabParams[i].idThread = i+1;
    tabParams[i].varPartagee = &predicat; 

    if (pthread_create(&threads[i], NULL, participant, &tabParams[i]) != 0){
      perror("erreur creation thread");
      exit(1);
    }
  }

  // attente de la fin des  threards. Partie obligatoire 
  for (int i = 0; i < atoi(argv[1]); i++){
    pthread_join(threads[i], NULL);
    }
  cout << "thread principal : fin de tous les threads secondaires" << endl;

  // terminer "proprement". 
    printf("Tous tes threads ont répondu, valeur de cpt : %i\n", cpt);

return 0;
}
 
