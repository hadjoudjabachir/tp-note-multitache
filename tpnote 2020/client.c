#include <stdio.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include<arpa/inet.h>
#include<string.h>


int main(int argc, char *argv[]) {

//Verifier la bonne utilisation du programme ./client adresse_ip_serveur port_serveur port_client 
  if (argc != 4){
    printf("utilisation : %s ip_serveur port_serveur port_client\n", argv[0]);
    exit(1);
  }

  /* Etape 1 : créer une socket avec le type stream utilise le protocole TCP */   
   int socketClient= socket(PF_INET, SOCK_STREAM, 0);

   //verifier la bonne création
  if (socketClient == -1){
      perror("Client : problème dans la creation de socket client :");
      exit(1);    // je choisis ici d'arrêter le programme car le reste
	                // dépend de la réussite de la création de la socket.
  }
  
  printf("Client : creation de la socket réussie ,appuyer sur entrée pour passer à la demande de connexion \n");
  getchar();
 
  /* Etape 2 : Nommer (initialiser / creer l'identité de la socket) la socket du client 
  struct sockaddr_in adrrClient;
  adrrClient.sin_family=AF_INET;
  adrrClient.sin_addr.s_addr=inet_addr("127.0.0.1");
  adrrClient.sin_port=htons((short) atoi(argv[3]));


  int res=bind(socketClient,(struct sockaddr*) &adrrClient,sizeof(adrrClient));
  


  //verfier la réussite du nommage
  if(res==-1)
 {
    perror("Client : probleme lors du nommage de la socket");
    exit(1);
 } 
   printf("Sosket bien nommé ,appuyer sur entrée pour passer à L'envoi du message ");
  
   getchar();
*/
   

  /* Etape 3 : Demander une connexion au serveur */
  struct sockaddr_in sockServ;
  sockServ.sin_family=AF_INET;
  inet_pton(AF_INET,argv[1],&(sockServ.sin_addr));
  sockServ.sin_port=htons((short) atoi(argv[2]));
  socklen_t lgA=sizeof(struct sockaddr_in);

   //demande de connexion 
   //on fait un casting, car les fonctions connect,listen, accept... demande le type sockaddr et non pas sockaddr_in
  int res=connect(socketClient,(struct sockaddr *)&sockServ,lgA);

  if(res==-1){
     perror("Client : erreur lors de la demande de connexion\n ");
     exit(1);
  }

  printf("la demande de connexion a bien réussi ,pressez entrée pour entamer l'echange\n  ");
  
  
  /* Etape 4 :Envoyer un message au serveur 
  Envoi d'une chaine de caractere 
  Reception d'un entier qui represente la taille du message réçu par le serveur
  */

  //utilisation de send au lieu de sendto 
  char message[1500];
  printf("Client envoie un message de maximum 100 caracteres !\n ");
  fgets(message,sizeof(message)-1, stdin); //je dois laisser une case pour la fin de chaine de caractère 

  //là je viens de récupérer le message qui est sous la forme chaine xxxx\n\0
  //                                                                 0123 4 5
  //                                                                 strlen =5
  // donc on remplace \n par \0    

  message[strlen(message)-1]='\0'; //remplacer le saut de ligne par une fin de chaine de caractère 
  
  size_t len_msg=strlen(message); //normalement il y avait un +1 qui contenait la \0 mais je l'ai enlevé pour ne pas prendre en compte \0
   //normalement len_msg =len(message)+1 comme ca on avait la place de rajouter un \0 
  /*
   programme client qui demande à un utilisateur de saisir une chaîne de caractères, envoie successivement deux
   fois cette chaîne (en effectuant deux appels de la fonction send(...) et sans inclure le caractère de fin de chaîne  '\0') et affiche le nombre
   total d'octets effectivement envoyés. La taille de la chaîne de caractères saisie au clavier
   ne dépassera pas 1500 caractères.
  */


  printf("strlen :%ld\n",strlen(message)); // la taille du message saisi
  printf("sizeof :%ld\n",sizeof(message)); // la taille qu'on a donné au buffer : 1500

  int envoi1= send(socketClient,message ,len_msg, 0); 
  
  if(envoi1 ==-1)
  {
     perror("Client : probleme lors de l'envoi du message\n ");
     exit(1);
  }else if(envoi1 == 0)
   {
     perror("Client : la socket a été fermé \n ");
     exit(1);
   }else if(envoi1 > len_msg){
     perror("Attention votre message dépasse la taille donc une partie est à recuperer plus tard \n");
  }

  int envoi2= send(socketClient,message ,len_msg, 0); 
  
  if(envoi2 ==-1)
  {
     perror("Client : probleme lors de l'envoi du message\n ");
     exit(1);
  }else if(envoi2 == 0)
   {
     perror("Client : la socket a été fermé \n ");
     exit(1);
   }else if(envoi2 > len_msg){
     perror("Attention votre message depasse la taille permise du buffer,une partie a été perdue\n");
  }


  printf("%d octets ont effectivement été deposé dans le buffer associé à la socket du client \n",envoi1+envoi2);
  printf("Envoi du message effectué.\n Appuyer sur Entrée pour recevoir un message de la part du serveur\n ");

  getchar();

 /*Etape 5 : reception du message de la part du serveur avec recv*/
 
   
   int rep;//la taille du message reçu genre salut : 5
   //int reception c'est le nombre d'octets d'un int : 4
   int reception=recv(socketClient,&rep,sizeof(rep),0); //sizeof(rep) c'est pareil comme sizeof(int) = 4
   if(rep != len_msg)
   {
     printf("Client erreur : la taille reçu de la part du serveur est differente de la taille du message envoyé\n ");
   } 
   
   if(reception == -1){ 
      perror("Client :erreur lors de la réception du message \n");
      exit(1);
   }else if (reception == 0){
      perror("Client :La socket a été fermée  \n");
      exit(1);
   }

   printf("%d est le nombre d'octets effectivement extrait\n ",reception);
   printf("message recu  est :%d \n Appuyez sur entrée pour fermer la socket\n  ",rep);
   getchar();
 
  
  /* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/
  close(socketClient);
  shutdown(socketClient,0);
  printf("Client : je termine\n");
  return 0;
}
//cette fonction on ne peut pas l employer que si on connait la taille qu on veut envoyer
//on donne l'emplacement du premier octet et le nombre d'octets que je ve eenvoyer
//LE COEUR DE la fonction ne doit pas traiter les erreurs c'est l appelant qui decide quoi faire ,elle elle ne fait que retourner des valeurs en cas d erreur

//on peut fonctionner soit avec le total deposé ou avec le restant a deposer 

//dans cette fonction on s en fout du tye de la donnée on la parcourt comme étant une suite d'octet
//si au lieu de void* msg ,j'ai char* msg il faut pas considerer le char comme un caractere ce n est pas le cas c'est similaire

//j'appelle cette fonction pour deposer un message en entier dont je connais la taille au prealable
//on utilise ca quand on commence a faire une succession de message car c'est là ou commence a se poser le  probleme 
//si c'est un sul envoi y aura pas de soucis 

/*
int sendTCP(int sock,void* msg,int sizeMsg)
{
   int totalDepose=0;
   while(totalDepose != sizeMsg)
   {
      int res=send(sock,msg+totalDepose,sizeMsg-totalDepose,0);
      if(res<=0) return res;
      totalDepose=totalDepose+res;
   }
   return 1;
}

//pareil pour le recvTCP 
///pour recv aussi on doit connaitre la taille qu'on va recevoir

int recvTCP(Int sock,void* msg,int sizeMsg)
{
   int totalRecu=0;
   while(totalRecu != sizeMsg)
   {
      int res=
   }
}*/
