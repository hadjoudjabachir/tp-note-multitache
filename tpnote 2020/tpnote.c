#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>


// Ce programme est à compléter en fonction des instructions à
// recevoir depuis un processus serveur TCP distant. L'IP et le numéro de
// port du serveur sont fournis en début d'épreuve par les enseignants
// et sont à passer en paramètres de votre programme.

// Une instruction est sous forme de chaine de caractères (caractère
// de fin de chaine inclus) dont la taille (de type int) est à
// recevoir juste avant le texte de l'instruction. Donc, une
// instruction implique deux messages : un premier message qui est la
// taille (en nombre d'octets) du texte de l'instruction et un second
// message qui est le texte de l'instruction.

// Après execution d'une instruction, votre programme recoit un
// feedback avec l'instruction suivante. Là aussi, deux messages sont
// à recevoir : le premier qui est la taille (de type int) en nombre
// d'octets du second mesage qui est une chaine de caractères
// (caractère de fin inclus).

// Remarque : un message de type chaine de caractères ne depassera jamais 6000 octets.
#define MAX_BUFFER_SIZE 6000


int main(int argc, char *argv[]) {

    if (argc != 4){
        printf("Utilisation : %s ip_serveur port_serveur param3 \n param3 est un entier dont la signification sera fournie par une instruction. En attendant cette instruction, passer n'importe quelle valeur\n", argv[0]);
        exit(1);
    }
    // première étape : se connecter au serveur et recevoir la première
    // instruction (lire commentaire plus haut). Bien évidement, il est
    // necessaire d'afficher le message reçu pour prendre connaissance
    // des instructions à suivre pour compléter votre programme.

    //Verifier la bonne utilisation du programme ./client adresse_ip_serveur port_serveur port_client 



  /* Etape 1 : créer une socket avec le type stream utilise le protocole TCP */   
   int socketClient= socket(PF_INET, SOCK_STREAM, 0);

   //verifier la bonne création
  if (socketClient == -1){
      perror("Client : problème dans la creation de socket client :");
      exit(1);    // je choisis ici d'arrêter le programme car le reste
	                // dépend de la réussite de la création de la socket.
  }
  
  printf("Client : creation de la socket réussie ,appuyer sur entrée pour passer à la demande de connexion \n");
  getchar();
 
  

  /* Etape 3 : Demander une connexion au serveur */
  struct sockaddr_in sockServ;
  sockServ.sin_family=AF_INET;
  inet_pton(AF_INET,argv[1],&(sockServ.sin_addr));
  sockServ.sin_port=htons((short) atoi(argv[2]));
  socklen_t lgA=sizeof(struct sockaddr_in);

   //demande de connexion 
   //on fait un casting, car les fonctions connect,listen, accept... demande le type sockaddr et non pas sockaddr_in
  int res=connect(socketClient,(struct sockaddr *)&sockServ,lgA);

  if(res==-1){
     perror("Client : erreur lors de la demande de connexion\n ");
     exit(1);
  }

  printf("la demande de connexion a bien réussi ,pressez entrée pour entamer l'echange\n  ");
    
    int taille2, taille3;

    int recvTaille2 = recv(socketClient, &taille2, sizeof(int), 0);
    printf("la taille recue est : %d \n", taille2);

    char message [5000];
    int reception = recv(socketClient, message, sizeof(message),0);
    printf("le message est : %s \n", message);
	
    int envoi = send(socketClient, message, strlen(message)+1, 0);
    
    int recvTaille3 = recv(socketClient, &taille3, sizeof(int), 0);
    int reception2 = recv(socketClient, message, taille3 ,0);
     printf("le message est : %s \n", message);
	



    int socketServer = socket(PF_INET, SOCK_STREAM, 0);

   if (socketServer == -1){
      perror("Serveur : pb creation socket :");
      exit(1); 
  }
  
  printf("Serveur : creation de la socket réussie ,appuyer sur entrée pour passer au nommage \n");
  getchar();
  
  /* Etape 2 : Nommer la socket du seveur */
  /*On commence par configurer l'adresse de mon serveur */
  struct sockaddr_in addrServer;
  addrServer.sin_family=AF_INET;
  addrServer.sin_addr.s_addr=INADDR_ANY; //on utilise l'adresse locale à mon pc car je fais des tests  en interne
  addrServer.sin_port=htons((short) atoi(argv[3]));

  res=bind(socketServer ,(const struct sockaddr*) &addrServer ,sizeof(addrServer));

  if(res==-1)
   {
    perror("Serveur :problème lors du nommage de la socket\n ");
    exit(1);
   } 

  printf("Socket bien nommé ,appuyez sur entrée pour passer la socket en mode écoute\n");
  getchar();

   /*Etape 3:passer la socket en mode écoute */

   int ecoute=listen(socketServer,10);
   if(ecoute==-1)
   {
      perror("Serveur TCP :erreur lors de la mise sur écoute \n");
      exit(1);
   }

   printf("le serveur a été mis sur ecoute avec succes , pressez entrée pour passer à l'accord de connexion \n ");
   getchar();
    short z = (short)atoi(argv[3]);
    printf("z: %i", z);
    int envoiPort = send(socketClient, &z, sizeof(z), 0);
   close(socketClient);

   
   struct sockaddr_in adClient;
   socklen_t lgA2=sizeof(adClient); 
   int dsClient=accept(socketServer,(struct sockaddr *)&adClient,&lgA2);
   
    int taille4;
    int recvTaille4 = recv(dsClient, &taille4, sizeof(int), 0);
    printf("la taille recue est : %d \n", taille4);

    char message2 [5000];
    int reception3 = recv(dsClient, message2, sizeof(message2),0);
    printf("le message est : %s \n", message2);
    
    /*
    int taille5;
    int recvTaille5 = recv(dsClient, &taille5, sizeof(int), 0);
    printf("la taille recue est : %d \n", taille5);

    char message3 [5000];
    int reception4 = recv(dsClient, message3, sizeof(message3),0);
    printf("le message est : %s \n", message3);
    
   */
    getchar();


   /*Etape 4 :Accepter une connexion */

  
/*
   char message5[4000]; 
   char str[INET_ADDRSTRLEN];  
   
   //serveur iteratif
   while(1)
   {

      struct sockaddr_in adClient;
       socklen_t lgA=sizeof(adClient);

       dsClient=accept(socketServer,(struct sockaddr *)&adClient,&lgA);

      if(dsClient==-1)
      {
         perror("Serveur TCP : erreur lors de l'acceptation de connexion ");
         continue;
      }

      inet_ntop(AF_INET,&adClient.sin_addr,str,INET_ADDRSTRLEN);
      printf("\n -----> Connexion Réussie au client à l'adresse %s:\n ",str);
      
      int id =fork();
      if(id==-1)
      {
         perror("Erreur lors du fork \n");
         exit(1);
      }else if(id==0)
      {
         //I am the child 
         close(socketServer);
        
        int taille5;
                int recvTaille5 = recv(dsClient, &taille5, sizeof(int), 0);
                        if(recvTaille5 ==-1)
                    {
                        perror("Serveur:erreur lors de la réception de la taille du message de la part du client ");
                        continue;
                    }else if( recvTaille5  == 0)
                    {
                        printf("Serveur : la socket a été fermé");
                        close(dsClient);
                    } 
            printf("la taille recue est : %d \n", taille5);


            int reception4 = recv(dsClient, message5, sizeof(message5),0);
                        if(reception4 ==-1)
                    {
                        perror("Serveur:erreur lors de la réception de la taille du message de la part du client ");
                        continue;
                    }else if( reception4  == 0)
                    {
                        printf("Serveur : la socket a été fermé");
                        close(dsClient);
                    } 
            printf("le message est : %s \n", message5);

         close(dsClient);
         printf("Socket fille fermée \n");
         exit(0);
      }else{
         printf("On est dans le parent et le pid parent est : \t %i",getpid());
      //I am the parent 
         close(dsClient);

      }
   }
  */
  /* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/

  printf("appuyez sur entrée pour fermer la socket \n ");
  getchar();
  close(socketServer);
  
	return 0;
}


/*Prochaine etape : le client devient aussi serveur TCP. 
A la suite de ce qui est deja fait,  mettre en place une socket d'ecoute TCP 
avec un numero de port a passer en parametre du programme (param3). Apres la 
mise en ecoute de cette socket, envoyer au serveur distant le numero de port 
choisi (type short) puis accepter une demande de connexion. Apres l'acceptation 
d'une demande de connexion d'un nouveau client, votre programme doit recevoir 
une nouvelle instruction envoyée par ce client. Ce n'est donc plus le serveur d
istant qui envoit des instructions mais un client (faire attention à utiliser 
la bonne socket). Vous devez fermer toute socket une fois qu'elle ne sera plus 
utile*/ 
