#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include<arpa/inet.h>
#include<string.h>


int main(int argc, char *argv[]) {

  

   int  res;
  if (argc != 2){
    printf("utilisation : %s port_serveur\n", argv[0]);
    exit(1);
   }

  /* Etape 1 : créer une socket */   
  int socketServer = socket(PF_INET, SOCK_STREAM, 0);

   if (socketServer == -1){
      perror("Serveur : pb creation socket :");
      exit(1); 
  }
  
  printf("Serveur : creation de la socket réussie ,appuyer sur entrée pour passer au nommage \n");
  getchar();
  
  /* Etape 2 : Nommer la socket du seveur */
  /*On commence par configurer l'adresse de mon serveur */
  struct sockaddr_in addrServer;
  addrServer.sin_family=AF_INET;
  addrServer.sin_addr.s_addr=INADDR_ANY; //on utilise l'adresse locale à mon pc car je fais des tests  en interne
  addrServer.sin_port=htons((short) atoi(argv[1]));

  res=bind(socketServer ,(const struct sockaddr*) &addrServer ,sizeof(addrServer));

  if(res==-1)
   {
    perror("Serveur :problème lors du nommage de la socket\n ");
    exit(1);
   } 

  printf("Socket bien nommé ,appuyez sur entrée pour passer la socket en mode écoute\n");
  getchar();

   /*Etape 3:passer la socket en mode écoute */

   int ecoute=listen(socketServer,10);
   if(ecoute==-1)
   {
      perror("Serveur TCP :erreur lors de la mise sur écoute \n");
      exit(1);
   }

   printf("le serveur a été mis sur ecoute avec succes , pressez entrée pour passer à l'accord de connexion \n ");
   getchar();
   
   /*Etape 4 :Accepter une connexion */
   struct sockaddr_in adClient;
   socklen_t lgA=sizeof(adClient);
   
   int dsClient=accept(socketServer,(struct sockaddr *)&adClient,&lgA);

   

   if(dsClient==-1)
   {
    perror("Serveur TCP : erreur lors de l'acceptation de connexion ");
    exit(1);
   }

   printf("accord de connexion réussi ,pressez entrée pour passer à la reception du message du client\n ");
   getchar();

  /* Etape 5 : recevoir un message du client:on sait au préalable que c'est une chaine de caractere de taille max 4000*/
   
  
   char message[4000]; 
   int reception=recv(dsClient,message,sizeof(message),0);
   
   message[strlen(message)]='\0'; 
   

   if(reception == -1){ 
      perror("Serveur :erreur lors de la réception du message\n ");
      exit(1);
   }
   printf("%d est le nombre d'octets effectivement extrait\n ",reception);


   printf("message recu :%s , appuyez sur entrée pour passer à l'envoi au client \n ",message);
   getchar();
 
  /* Etape 6 : envoyer un message au client (voir sujet pour plus de détails)*/
   

  res= send(dsClient,&reception,sizeof(reception), 0); 
  if(res==-1)
  {
     perror("Serveur : probleme lors de l'envoi du message \n");
     exit(1);

  }else if (res==0) {
     printf("Serveur TCP :la socket a été fermée");

  }  
   printf("le serveur a envoyé le message suivant au client %d ,appuyez sur entrée pour fermer la socket",reception);
   getchar();
  /* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/
  close(socketServer);
  close(dsClient);

  printf("Serveur : je termine\n");
  return 0;
}
