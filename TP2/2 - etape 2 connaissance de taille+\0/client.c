#include <stdio.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include<arpa/inet.h>
#include<string.h>


int main(int argc, char *argv[]) {

//Verifier la bonne utilisation du programme ./client adresse_ip_serveur port_serveur port_client 
  if (argc != 4){
    printf("utilisation : %s ip_serveur port_serveur port_client\n", argv[0]);
    exit(1);
  }

  /* Etape 1 : créer une socket avec le type stream utilise le protocole TCP */   
   int socketClient= socket(PF_INET, SOCK_STREAM, 0);

   //verifier la bonne création
  if (socketClient == -1){
      perror("Client : problème dans la creation de socket client :");
      exit(1);    // je choisis ici d'arrêter le programme car le reste
	                // dépend de la réussite de la création de la socket.
  }
  
  printf("Client : creation de la socket réussie ,appuyer sur entrée pour passer à la demande de connexion \n");
  getchar();
 
   

  /* Etape 3 : Demander une connexion au serveur */
  struct sockaddr_in sockServ;
  sockServ.sin_family=AF_INET;
  inet_pton(AF_INET,argv[1],&(sockServ.sin_addr));
  sockServ.sin_port=htons((short) atoi(argv[2]));
  socklen_t lgA=sizeof(struct sockaddr_in);

   //demande de connexion 
   //on fait un casting, car les fonctions connect,listen, accept... demande le type sockaddr et non pas sockaddr_in
  int res=connect(socketClient,(struct sockaddr *)&sockServ,lgA);

  if(res==-1){
     perror("Client : erreur lors de la demande de connexion\n ");
     exit(1);
  }

  printf("la demande de connexion a bien réussi ,pressez entrée pour entamer l'echange\n  ");
  
  
  /* Etape 4 :Envoyer un message au serveur 
  Envoi d'une chaine de caractere 
  Reception d'un entier qui represente la taille du message réçu par le serveur
  */

  //utilisation de send au lieu de sendto 
  char message[1500];
  printf("Client envoie un message de maximum 100 caracteres !\n ");
  fgets(message,sizeof(message), stdin); //je dois laisser une case pour la fin de chaine de caractère 

  message[strlen(message)-1]='\0'; //remplacer le saut de ligne par une fin de chaine de caractère 
  
  size_t len_msg=2*(strlen(message)+1); 

  int envoitaille= send(socketClient,&len_msg ,sizeof(int), 0); 
  
  if(envoitaille ==-1)
  {
     perror("Client : probleme lors de l'envoi du message\n ");
     exit(1);
  }else if(envoitaille == 0)
   {
     perror("Client : la socket a été fermé \n ");
     exit(1);
   }

  int envoi1= send(socketClient,message ,strlen(message)+1, 0); 
  
  if(envoi1 ==-1)
  {
     perror("Client : probleme lors de l'envoi du message\n ");
     exit(1);
  }else if(envoi1 == 0)
   {
     perror("Client : la socket a été fermé \n ");
     exit(1);
   }

  int envoi2= send(socketClient,message ,strlen(message)+1, 0); 
  
  if(envoi2 ==-1)
  {
     perror("Client : probleme lors de l'envoi du message\n ");
     exit(1);
  }else if(envoi2 == 0)
   {
     perror("Client : la socket a été fermé \n ");
     exit(1);
   }


  printf("%d octets ont effectivement été envoyé \n",envoi1+envoi2);
  printf("Envoi du message effectué.\n Appuyer sur Entrée pour recevoir un message de la part du serveur\n ");

  getchar();

 /*Etape 5 : reception du message de la part du serveur avec recv*/
 
   
   int rep;
   int reception=recv(socketClient,&rep,sizeof(rep),0); //sizeof(rep) c'est pareil comme sizeof(int) = 4
   if(rep != len_msg)
   {
     printf("Client erreur : la taille reçue de la part du serveur est differente de la taille du message envoyé\n ");
   } 
   
   if(reception == -1){ 
      perror("Client :erreur lors de la réception du message \n");
      exit(1);
   }else if (reception == 0){
      perror("Client :La socket a été fermée  \n");
      exit(1);
   }

   printf("message recu  est :%d \n Appuyez sur entrée pour fermer la socket\n  ",rep);
   getchar();
 
  
  /* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/
  close(socketClient);
  printf("Client : je termine\n");
  return 0;
}