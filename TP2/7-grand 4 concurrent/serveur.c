#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include<arpa/inet.h>
#include<string.h>
 

/*Serveur Iteratif*/

/*Adresse IP : 91.174.102.81

Numéro de port : 32768 */
/* Programme serveur */

int recvTCP(int sock,void* msg,int sizeMsg)
{
   int received=0;
   int res;

   while(received < sizeMsg)
   {
      res=recv(sock,msg+received,sizeMsg-received,0);
      received=received+res;
      if(res<=0)
      {
        return res;
      } 
   }
   return 1;
}


int main(int argc, char *argv[]) {

 
  int  res;
  if (argc != 2){
    printf("utilisation : %s port_serveur\n", argv[0]);
    exit(1);
   }

  /* Etape 1 : créer une socket */   

  int socketServer = socket(PF_INET, SOCK_STREAM, 0);

   if (socketServer == -1){
      perror("Serveur : pb creation socket :");
      exit(1); 
  }
  
  printf("Serveur : creation de la socket réussie\n");

  
  /* Etape 2 : Nommer la socket du seveur */
  
  struct sockaddr_in addrServer;
  addrServer.sin_family=AF_INET;
  addrServer.sin_addr.s_addr=INADDR_ANY; //on utilise l'adresse locale à mon pc car je fais des tests  en interne
  addrServer.sin_port=htons((short) atoi(argv[1]));

  res=bind(socketServer ,(const struct sockaddr*) &addrServer ,sizeof(addrServer));

  if(res==-1)
   {
    perror("Serveur :problème lors du nommage de la socket\n ");
    exit(1);
   } 

   printf("Socket nommée avec Succes \n ");

   //afficher le port 
   socklen_t sizeAd=sizeof(addrServer);
   getsockname(socketServer,(struct sockaddr*) &addrServer,&sizeAd);
   printf("port : %i\n ",ntohs(addrServer.sin_port));


   /*Etape 3:passer la socket en mode écoute */

   int reslisten=listen(socketServer,5);
   if(reslisten==-1)
   {
      perror("Serveur TCP :erreur lors de la mise sur écoute \n");
      exit(1);
   }

   printf("Serveur listenning ..\n  ");
   printf("appuyez sur entrée pour accorder l'accord de connexion\n ");   
   getchar();

   /*Etape 4 :Accepter une connexion */
  
  
   int dsClient;
   char message[4000]; 
   char str[INET_ADDRSTRLEN];  
   
   //serveur iteratif
   while(1)
   {
       struct sockaddr_in adClient;
       socklen_t lgA=sizeof(adClient);
       
       dsClient=accept(socketServer,(struct sockaddr *)&adClient,&lgA);


      if(dsClient==-1)
      {
         perror("Serveur TCP : erreur lors de l'acceptation de connexion ");
         continue;
      }

      inet_ntop(AF_INET,&adClient.sin_addr,str,INET_ADDRSTRLEN);
      printf("\n -----> Connexion Réussie au client à l'adresse %s:\n ",str);
      
      int id =fork();
      if(id==-1)
      {
         perror("Erreur lors du fork \n");
         exit(1);
      }else if(id==0)
      {
         //I am the child 
         close(socketServer);
         printf("I am the child process , mon id est %i\n",getpid());
         /* Etape 5 : reception des messages du client (int representant la taille)  */

         //recevoir une taille de message à lire
         int taille;
         res=recvTCP(dsClient,&taille,sizeof(taille));

         if(res==-1)
         {
            perror("Serveur:erreur lors de la réception de la taille du message de la part du client ");
            continue;
         }else if( res == 0)
         {
            printf("Serveur : la socket a été fermé");
            continue;
          } 

         
         printf("%i :Taille du prochain message : \t  %i\n " ,getpid(),taille);
        

      
         //recevoir  le message 

         int res=recvTCP(dsClient,message,taille);
      

         if(res== -1){ 
               perror("Serveur :erreur lors de la réception du message\n ");
               continue;
         }else if(res ==0)
            {
               printf("Serveur :la socket a été fermée par la couche transport \n ");
               continue;
            } 
         printf("%i Message :%s \n",getpid(),message);

         //j'ai fini de traiter le client en cours donc je le ferme 


         close(dsClient);
         printf("Socket fille fermée \n");
         exit(0);
      }else{
         printf("On est dans le parent et le pid parent est : \t %i",getpid());
      //I am the parent 
         close(dsClient);

      }
   }
   
  
  /* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/

  printf("appuyez sur entrée pour fermer la socket \n ");
  getchar();
  close(socketServer);
  

  printf("Serveur : je termine\n");
  return 0;
}