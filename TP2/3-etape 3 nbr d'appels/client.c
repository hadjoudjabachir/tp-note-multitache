#include <stdio.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include<arpa/inet.h>
#include<string.h>


int main(int argc, char *argv[]) {

//Verifier la bonne utilisation du programme ./client adresse_ip_serveur port_serveur port_client 
  if (argc != 5){
    printf("utilisation : %s ip_serveur port_serveur port_client nbr_iterations\n", argv[0]);
    exit(1);
  }

  /* Etape 1 : créer une socket avec le type stream utilise le protocole TCP */   
   int socketClient= socket(PF_INET, SOCK_STREAM, 0);

   //verifier la bonne création
  if (socketClient == -1){
      perror("Client : problème dans la creation de socket client :");
      exit(1);    // je choisis ici d'arrêter le programme car le reste
	                // dépend de la réussite de la création de la socket.
  }
  
  printf("Client : creation de la socket réussie ,appuyer sur entrée pour passer à la demande de connexion \n");
  getchar();
   

  /* Etape 3 : Demander une connexion au serveur */
  struct sockaddr_in sockServ;
  sockServ.sin_family=AF_INET;
  inet_pton(AF_INET,argv[1],&(sockServ.sin_addr));
  sockServ.sin_port=htons((short) atoi(argv[2]));
  socklen_t lgA=sizeof(struct sockaddr_in);

   //demande de connexion 
  int res=connect(socketClient,(struct sockaddr *)&sockServ,lgA);

  if(res==-1){
     perror("Client : erreur lors de la demande de connexion\n ");
     exit(1);
  }

  printf("la demande de connexion a bien réussi ,pressez entrée pour entamer l'echange\n  ");
  
  
  /* Etape 4 :Envoyer un message au serveur 
  Envoi d'une chaine de caractere 
  Reception d'un entier qui represente la taille du message réçu par le serveur
  */

  //utilisation de send au lieu de sendto 
  char message[1500];
  printf("Client envoie un message de maximum 100 caracteres !\n ");
  fgets(message,sizeof(message)-1, stdin); //je dois laisser une case pour la fin de chaine de caractère 

  message[strlen(message)-1]='\0'; 
  
  size_t len_msg=strlen(message);


   int OctetsTotal, OctetsSupposes, Nbr_appel;
   Nbr_appel = (atoi(argv[4]));

   for(int i=0; i < Nbr_appel; i++){
      int envoi= send(socketClient,message ,len_msg, 0); 
      OctetsTotal = OctetsTotal+envoi;
  
         if(envoi ==-1)
      {
         perror("Client : probleme lors de l'envoi du message\n ");
         exit(1);
      }else if(envoi == 0)
      {
         perror("Client : la socket a été fermé \n ");
         exit(1);
      }
      
      OctetsSupposes = len_msg * (atoi(argv[4]));

}
  printf(" le nombre d'octets supposés etre envoyés: %d\n", OctetsSupposes);
  printf(" le nombre d'octets total envoyés après la 1ere emission: %d\n", OctetsTotal);
  printf("Envoi du message effectué.\n Appuyer sur Entrée pour recevoir un message de la part du serveur\n ");

  getchar();

 
  
  /* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/
  close(socketClient);
  printf("Client : je termine\n");
  return 0;
}