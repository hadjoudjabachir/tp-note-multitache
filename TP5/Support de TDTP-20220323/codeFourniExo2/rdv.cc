#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdlib.h>
//#include "calcul.h"
/*
 exemple de crétaion d'un tableau de sémaphores, dont le nombre
 d'éléments et la valeur initiale est passée en paramétre du
 programme (dans cet exemple, les élements sont initialisés é la méme valeur)

 */

/* Le fichier SimInt sert à créer  un tableau de sémaphores et nous on doit créer un tableau de 1 sémaphore qui est intitialisé au nombre de processus qu'on a,
Exemple : si on a 3 processus, on crée un tableau de taille 1 avec valeur 3: 
notre sémaphore - > [|3|]
Processus 1 : 
WorkP1-1()
P(1) -> |2|
Z(sem) -> il se met en attente que la valeur du sémaphores atteigne 0
WorkP1_2()

Processus 2 : 
WorkP2-1()
P(1)   -> |1|
Z(sem)  -> el il se met en attente que le sem atteigne la valeur 0
WorkP2_2()

Processus 3 :
WorkP3-1()
P(1) -> |0|
Z(sem) -> il se met en attente normalement mais vu que la valeur est déjà à 0 il attend personne vu que c'est le dernier attendu, et tous les autres se réveillent automatiquement et commencent leur WorkPi_2()
WorkP3_2()
*/

int main(int argc, char * argv[]){
  
  if (argc!=3) {
    printf("Nbre d'args invalide, utilisation :\n");
    printf("%s fichier-pour-cle-ipc entier-pour-clé-ipc\n", argv[0]);
    exit(0);
  }
	  
  int cle = ftok(argv[1], atoi(argv[2]));

  int idSem;
  if((idSem = semget(cle,1,0666))== -1){; //crée et retourne l'id du tableau du semaphore
    perror("erreur semget : ");
    exit(-1);
  }

  printf("job %d commence le travail\n", getpid());
  srand(200);
  sleep(1);

  //point rdv
  printf("job %d arrive au rdv\n",getpid());

  //operation p et v explicites
  struct sembuf p;
  p.sem_num = 0;
  p.sem_op = -1;
  p.sem_flg = 0;

  struct sembuf z;
  p.sem_num = 0;
  p.sem_op = 0;
  p.sem_flg = 0;

  //mes operation p et z version tableau
  struct sembuf op[] = {{0,-1,0},{0,0,0}};

  //p(1)
  if(semop(idSem,op,1)== -1){
      perror("erreur semop\n");
      exit(1);
  }
    //z
    if(semop(idSem,op+1,1)== -1){
      perror("erreur semop\n");
      exit(1);
  }

  printf("job %d reprise le travail\n",getpid());

  return 0;

}