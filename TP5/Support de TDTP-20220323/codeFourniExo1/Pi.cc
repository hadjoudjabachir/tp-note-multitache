#include <stdlib.h>
#include <sys/types.h>
#include <iostream>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdio.h>//perror
#include <unistd.h>
#include "calcul.h"
using namespace std;

// pour supprimer la file ayant la clé passée en parametre.

// une autre solution est d'utiliser la commande ipcrm -q <id_file>
// après avoir obtenue l'id de la file via la commande ipcs.

int main(int argc, char * argv[]){

  if (argc!=3) {
    cout<<"Nbre d'args invalide, utilisation :"<< endl;
    cout << argv[0] << " fichier-pour-cle-ipc entier-pour-cle-ipc"<< endl;
    exit(0);
  }
	  
  key_t cle=ftok(argv[1], atoi(argv[2]));

  if (cle==-1) {
    perror("Erreur ftok : ");
    exit(2);
  }

  cout << "ftok ok" << endl;
    
  int msgid = msgget(cle, 0666);
  if(msgid==-1) {
    perror("erreur msgget : ");
    exit(2);
  }
  
  cout << "msgget ok" << endl;

  while (1){
    // envoi demande d'acces
    struct demandeAcces {
      long etiq;   // 1 : demande d'acces
      int idDemandeur;
    } demande;

      int pid = getpid();
      demande.etiq = 1;
      demande.idDemandeur = pid;
    cout <<pid<< " : Envoi demande autorisation d'acces"<<endl;
    if (msgsnd(msgid, (void *)&demande, sizeof(int),0)==-1){ //un parametre de plus que le send, l'étiquette en plus
      perror("Erreur envoi demande acces :");
      exit(2);
    }

    //attente autorisation
    struct sautorisation{
      long etiq; // id du demandeur
      char valeur; //histoire d'envoyer quelque chose
    }autorisation;

    cout <<pid<< " : Attente autorisation d'acces"<<endl;
    if (msgrcv(msgid, (void *)&autorisation, sizeof(char), long(pid), 0)==-1){
      perror("Reception autorisation :");
      exit (2);
    }
    cout<<pid<<" : Autorisation obtenue, je travaille"<<endl;
    //un travail simulant l'étape d'utilisation de la ressource
    //calcul(atoi(argv[3]));

    // envoi notification de liberation
    struct finAcces{
      long etiq;  // 2 : fin d'acces
      int idDemandeur; // ça peut etre autre chose
    } fin;

    fin.etiq = 2;
    fin.idDemandeur = getpid();
    
    cout<<pid<<": Envoi notification de fin d'acces"<<endl;
    cout<<"Attente libération de la ressource"<<endl;
    if(msgsnd(msgid, (void *)&fin,sizeof(int), 0)==-1){
      perror("Erreur envoi notification fin d'acces : ");
      exit(2);
    }
  }

  return 0;  

}
