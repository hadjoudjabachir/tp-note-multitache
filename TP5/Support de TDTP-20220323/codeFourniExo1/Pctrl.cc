#include <stdlib.h>
#include <sys/types.h>
#include <iostream>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdio.h>//perror
#include <unistd.h>
using namespace std;



int main(int argc, char * argv[]){

  if (argc!=3) {
    cout<<"Nbre d'args invalide, utilisation :"<< endl;
    cout << argv[0] << " fichier-pour-cle-ipc entier-pour-cle-ipc"<< endl;
    exit(0);
  }
	  
  key_t cle=ftok(argv[1], atoi(argv[2]));

  if (cle==-1) { 
    perror("Erreur ftok : ");
    exit(2);
  }

  cout<< "ftok ok" << endl;
    
  int msgid;
  if((msgid = msgget(cle, IPC_CREAT|IPC_EXCL|0666))==-1) { //création d’un file nouvelle de cle « clenv » avec des 
                                                          //accès en lecture/écriture pour tous
    perror("erreur msgget : ");
    exit(2);
  }
  
  cout << "msgget ok" << endl;
  while (1){
    // reception demande d'acces
    struct demandeAcces {
      long etiq;   // 1 : demande d'acces
      int idDemandeur;
    } demande;
    // Règle générale : PAS DE POINTEUR DANS LA STRUCTURE !!!
    // L'autre processus ne pourrait pas y accéder !

    cout << "Attente demande d'acces"<<endl;
    if (msgrcv(msgid, (void *)&demande, sizeof(int), long(1),0)==-1){
      perror("Reception demande autorisation :");
      exit(2);
    }
    cout<<"Demande d'acces reçue :"<<endl;
    cout<<"etiquette :"<<demande.etiq<<"  ";
    cout<<"id demandeur :"<<demande.idDemandeur<<endl;

    //envoyer autorisation
    struct sautorisation{
      long etiq; // id du demandeur
      char valeur; //histoire d'envoyer quelque chose
    }autorisation;


    autorisation.etiq = demande.idDemandeur;
    autorisation.valeur = 'a';

    if (msgsnd(msgid, (void *)&autorisation, sizeof(char), 0)==-1){
      perror("Erreur envoi autorisation :");
      exit (2);
    }
    printf("Autorisation bien envoyée");
    // reception notification de liberation
    struct finAcces{
      long etiq;  // 2 : fin d'acces
      int idDemandeur; // ça peut etre autre chose
    } fin;

    cout<<"Attente libération de la ressource"<<endl;
    if(msgrcv(msgid, (void *)&fin,sizeof(int), long(2),0)==-1){
      perror("Reception notification fin d'acces : ");
      exit(2);
    } 
    cout<<"contenu du message lu :"<<endl;
    cout<<"etiquette :"<<fin.etiq<<"  ";
    cout<<"id demandeur :"<<fin.idDemandeur<<endl;
  }
  return 0;
}

