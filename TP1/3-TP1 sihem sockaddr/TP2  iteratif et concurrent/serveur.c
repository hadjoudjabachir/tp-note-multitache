#include <stdio.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include<arpa/inet.h>
#include<string.h>

/* Programme serveur */
int sendTCP(int sock,char* msg, int length){
   ssize_t sent;
   int total_sent=0;
   while(length>0){
      sent = send(sock,msg,length,0);
      if(sent==-1){
         perror("client : erreur a l'envoi\n ");
      } 
      msg+=sent;
      total_sent+=sent;
      length-=sent;
   } 
   return total_sent;
} 

int recvTCP(int sock, char* msg, int length){
   ssize_t received;
   int total_recv=0;
   while(length>0){
      received = recv(sock,msg,length,0);
      if(received==-1){
         perror("client : erreur a la reception\n ");
      } 
      msg-=received;
      total_recv+=received;
      length-=received;
   } 
   return total_recv;
} 

int main(int argc, char *argv[]) {

  /* Je passe en paramètre le numéro de port qui sera donné à la socket créée plus loin.*/

  /* Je teste le passage de parametres. Le nombre et la nature des
     paramètres sont à adapter en fonction des besoins. Sans ces
     paramètres, l'exécution doit être arrétée, autrement, elle
     aboutira à des erreurs.*/
  if (argc != 2){
    printf("utilisation : %s port_serveur\n", argv[0]);
    exit(1);
  }

  /* Etape 1 : créer une socket */   
  int ds = socket(PF_INET, SOCK_STREAM, 0);

  /* /!\ : Il est indispensable de tester les valeurs de retour de
     toutes les fonctions et agir en fonction des valeurs
     possibles. Voici un exemple */
  if (ds == -1){
    perror("Serveur : pb creation socket :");
    exit(1); // je choisis ici d'arrêter le programme car le reste
	     // dépendent de la réussite de la création de la socket.
  }
  
  /* J'ajoute des traces pour comprendre l'exécution et savoir
     localiser des éventuelles erreurs */
  printf("Serveur : creation de la socket réussie \n");
  
  // Je peux tester l'exécution de cette étape avant de passer à la
  // suite. Faire de même pour la suite : n'attendez pas de tout faire
  // avant de tester.
  
  /* Etape 2 : Nommer la socket du seveur */
  struct sockaddr_in adServ;
  adServ.sin_family = AF_INET;
  adServ.sin_addr.s_addr = INADDR_ANY;
  adServ.sin_port = htons(atoi(argv[1]));
  int res = bind(ds,(struct sockaddr*)&adServ, sizeof(adServ));

  if(res==-1){
     perror("serveur : erreur au nommage\n");
  }
  else printf("serveur : nommage reussi\n");
 
  /*passer en mode ecoute*/
  int ecoute = listen(ds,10);
  if(ecoute==-1){
     perror("serveur : n'a pas pu passer en mode ecoute");
     exit(1);
  } 
  

  
  /*accepter la demande de connexion*/
  /*struct sockaddr_in sClient;
  socklen_t lAdC = sizeof(struct sockaddr_in);
  int acpt= accept(ds,(struct sockaddr*)&sClient, &lAdC);

  if(acpt<0){
     perror("serveur : demande refusée\n ");
     exit(1);
  } 
  else printf("demande de connexion acceptée\n ");*/



  /* Etape 4 : recevoir un message du client (voir sujet pour plus de détails)*/
  
  
  //m[strlen(m)+1]='\0';
  /*int cptrecv=0;
  int rcv;
  while(1){ 
  rcv = recvTCP(acpt,m,sizeof(m));

  if(rcv==0){
     perror("serveur : socket fermée\n");
  }
  else if(rcv==-1){
     perror("serveur : erreur a la reception\n" );
  } 
  cptrecv++;
  printf("serveur : nombres d'octets recu : %d \n", rcv);
  printf("serveur : nb d'appel a la fonction recv : %d \n", cptrecv);
  } */

/*serveur iteratif*/
  while(1){
     char* m[200];
     struct sockaddr_in sClient;
     socklen_t lAdC = sizeof(struct sockaddr_in);
     int acpt= accept(ds,(struct sockaddr*)&sClient, &lAdC);
     ssize_t rcv=recv(acpt,m,sizeof(m),0);
     if(rcv==-1){
        perror("serveur : erreur a la reception");
     } 
     else printf("serveur : message bien reçu : %s\n",m);
     printf("serveur : nombres d'octets recu : %d \n", (int)rcv);

     int sen=sendTCP(acpt,&rcv,sizeof(int));
     if(sen<0){
     perror("serveur : erreur a l'envoi\n");
     }
     printf("serveur : message bien envoyé\n");
     close(acpt);
       } 
  
  /*serveur concurrent
  printf("saisir un truc\n ");
  char t[100] ;
  scanf("%s",t) ;
  while(1){
     struct sockaddr_in sClient;
     socklen_t lAdC = sizeof(struct sockaddr_in);
     int acpt= accept(ds,(struct sockaddr*)&sClient, &lAdC);
     char* m[200]; 
     int PID = fork();
     if(PID<0){
        perror("serveur : erreur dans le fork\n ");
        exit(1);
     }
     else if (PID==0){ 
     int rcv = recv(acpt,m,strlen(m)+1,0);
     if(rcv<0){
        perror("serveur : erreur a la reception\n ");
     } 
     printf("serveur : message recu : %s\n",m);
     printf("serveur : nb d'octets recu : %d\n",rcv);

     ssize_t sent=sendTCP(acpt,&rcv,sizeof(int));
     if(sent==-1){
        perror("serveur : erreur a l'envoi\n ");
     } 
     printf("serveur : message envoyé\n ");
     }else{
        close(acpt);
     }  

  } */


  
  /* Etape 5 : envoyer un message au client (voir sujet pour plus de détails)
 int sen=sendTCP(acpt,&rcv,sizeof(int));

  if(sen<0){
     perror("serveur : erreur a l'envoi\n");
  }
  else printf("serveur : message bien envoyé\n");*/

  /* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/
  
  
  close(ds);
  printf("Serveur : je termine\n");
  return 0;
}
