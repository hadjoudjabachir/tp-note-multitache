#include <stdio.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include<arpa/inet.h>
#include<string.h>

/* Programme client */

int main(int argc, char *argv[]) {

  /* je passe en paramètre l'adresse de la socket du serveur (IP et
     numéro de port) et un numéro de port à donner à la socket créée plus loin.*/

  /* Je teste le passage de parametres. Le nombre et la nature des
     paramètres sont à adapter en fonction des besoins. Sans ces
     paramètres, l'exécution doit être arrétée, autrement, elle
     aboutira à des erreurs.*/
  if (argc != 4){
    printf("utilisation : %s ip_serveur port_serveur port_client\n", argv[0]);
    exit(1);
  }

  /* Etape 1 : créer une socket */   
  int ds = socket(PF_INET, SOCK_STREAM, 0);

  /* /!\ : Il est indispensable de tester les valeurs de retour de
     toutes les fonctions et agir en fonction des valeurs
     possibles. Voici un exemple */
  if (ds == -1){
    perror("Client : pb creation socket :");
    exit(1); // je choisis ici d'arrêter le programme car le reste
	     // dépendent de la réussite de la création de la socket.
  }
  
  /* J'ajoute des traces pour comprendre l'exécution et savoir
     localiser des éventuelles erreurs */
  printf("Client : creation de la socket réussie \n");
  
  // Je peux tester l'exécution de cette étape avant de passer à la
  // suite. Faire de même pour la suite : n'attendez pas de tout faire
  // avant de tester.
  

  /* Etape 2 : Nommer la socket du client */
  struct sockaddr_in ad;
  ad.sin_family = AF_INET;
  ad.sin_addr.s_addr = INADDR_ANY;
  ad.sin_port = htons((short)atoi(argv[3]));
  int res=bind(ds,(struct sockaddr*)&ad, sizeof(ad));

  if(res==-1){
     perror("Client : erreur au nommage de la socket \n");
  }
  

  /* Etape 3 : Désigner la socket du serveur */
  struct sockaddr_in adServ;
  adServ.sin_family = AF_INET;
  adServ.sin_addr.s_addr = inet_addr(argv[1]);
  adServ.sin_port = htons((short)atoi(argv[2]));

  /*demande de connexion*/
  socklen_t lgAdser= sizeof(struct sockaddr_in);
  int cnx = connect(ds,(struct sockaddr*)&adServ, lgAdser);

  if(cnx==-1){
     perror("client : la connexion a echoué");
  } 
  else printf("client : connexion reussie\n");

  char m[100];
  printf("client : saisir un msg a envoyer \n");
  scanf("%s",m);
  //m[strlen(m)+1]='\0';

  /* Etape 4 : envoyer un message au serveur  (voir sujet pour plus de détails)*/
  
  ssize_t sen = send(ds,m, strlen(m)+1,0);

  if(sen==-1){
     perror("Client : erreur à l'envoi\n");
     exit(1);
  }
  else if(sen==0){
     perror("client : socket fermée\n ");
  } 
  else printf("Client : message1 bien envoyé\n");

  ssize_t sen1 = send(ds,m, strlen(m)+1,0);
  if(sen1==-1){
     perror("Client : erreur à l'envoi\n");
     exit(1);
  }
  else if(sen1==0){
     perror("client : socket fermée\n ");
  } 
  else printf("Client : message2 bien envoyé\n");
  printf("nb d'octets envoyés : %d\n",(int)sen1);
  
  
  /* Etape 5 : recevoir un message du serveur (voir sujet pour plus de détails)*/
  int msg;
  ssize_t rcv = recv(ds,&msg,sizeof(msg),0);

  if(rcv<0){
     perror("Client : erreur a la reception\n");
  }
  if(msg==strlen(m)+1){
   printf("Client : message reçu meme taille : %d\n",msg);}
  
  
  /* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/
  
  
  printf("Client : je termine\n");
  return 0;
}
